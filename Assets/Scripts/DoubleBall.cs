﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;
using UnityEngine.UI;

public class DoubleBall : MonoBehaviour
{
    public GameObject pongBallClone;
    public Transform cloneSpawn;

    public Text puPlayer1Text;
    public Text puPlayer2Text;
    public Text puPlayer1ScoreText;
    public Text puPlayer2ScoreText;

    void Start()
    {
        pongBallClone.GetComponent<PongBallMove>();
    }

    void OnTriggerEnter(Collider col)
    {
        if (col.CompareTag("ball"))
        {
            GameObject ballClone = Instantiate(pongBallClone, cloneSpawn.position, Quaternion.identity);

            ballClone.GetComponent<Score>().player1Text = puPlayer1Text;
            ballClone.GetComponent<Score>().player2Text = puPlayer2Text;
            ballClone.GetComponent<Score>().player1ScoreText = puPlayer1ScoreText;
            ballClone.GetComponent<Score>().player2ScoreText = puPlayer2ScoreText;

            Destroy(gameObject);
        }
    }
}
