﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class MovementRight : MonoBehaviour
{
    public GameObject rightPlayer;
    public float moveSpeed;
    public bool isPaused = false;

    public Audio_Manager audioManager;
    public AudioClip pongRightSound;

    private int minY = 2, maxY = 12;

    /*void Start()
    {
        audioManager = GameObject.FindWithTag("AudioManager").GetComponent<Audio_Manager>();
    }*/

    void Update ()
    {
        if (isPaused == false)
        {
            float verticalInput = Input.GetAxis("P2_VerticalAxis") * moveSpeed * Time.deltaTime;

            transform.Translate(0f, verticalInput, 0f);

            Vector3 tempVector = transform.position;

            tempVector.y = Mathf.Clamp(tempVector.y, minY, maxY);

            transform.position = tempVector;
        }
     }

    /*void OnCollisionEnter(Collision col)
    {
        audioManager.PlaySFX(pongRightSound);
    }*/
}
